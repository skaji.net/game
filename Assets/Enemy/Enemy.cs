﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    //Variables
    public float health;
    public float pointsToGive;

    public float waitTime;

    private float currentTime;
    private bool shot;

    public GameObject bullet;

    public GameObject bulletSpawnPoint;
    public Transform bulletSpawned;
    public Player player;

    //Methods
    public void Start()
    {      
        player = GameObject.FindWithTag("Player").GetComponent<Player>();
        bulletSpawnPoint = GameObject.Find("PistolHolder/Pistol/bulletSpawnPoint");
    }

    public void FixedUpdate()
    {
        if (health <= 0)
        {
            Die();
        }

        this.transform.LookAt(player.transform);
        if (currentTime == 0)
            Shoot();

        if (shot && currentTime < waitTime)
            currentTime += 1 * Time.deltaTime;
        
        if (currentTime >= waitTime)
            currentTime = 0;
    }

    public void Die()
    {
        Destroy(gameObject);
        player.points += pointsToGive;
    }

    public void Shake()
    {
        transform.Translate(Time.fixedDeltaTime * 20f * Vector3.back);
    }

    public void Shoot()
    {
        shot = true;
        bulletSpawned = Instantiate(bullet.transform, bulletSpawnPoint.transform.position, Quaternion.identity);
        bulletSpawned.rotation = this.transform.rotation;
    }
}
