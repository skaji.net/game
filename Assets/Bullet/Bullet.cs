﻿using UnityEngine;


public class Bullet : MonoBehaviour
{
    //Variables
    public float speed;
    public float maxDistance;
    private GameObject _triggeringEnemy;
    public float damage;

    //Methods
    void Update()
    {
        transform.Translate(Time.deltaTime * speed * Vector3.forward);
        maxDistance += 1 * Time.deltaTime;

        if (maxDistance >= 2)
        {
            Destroy(this.gameObject);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            _triggeringEnemy = other.gameObject;
            _triggeringEnemy.GetComponent<Enemy>().health -= damage;
            _triggeringEnemy.GetComponent<Enemy>().Shake();
            Destroy(gameObject);
        }
    }
}