﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //Variables
    public float movementSpeed;
    public float jumpHeight;
    public GameObject camera;
    public GameObject playerObj;
    public GameObject bulletSpawnPoint;
    public GameObject bullet;
    private float _lastShoot, timeBetweenShoots = 0.5f;
    public float points;
    private Transform _bulletSpawned;

    //Methods
    public void FixedUpdate()
    {
        //Player facing mouse
        Plane playerPlane = new Plane(Vector3.up, transform.position);
        Ray ray = UnityEngine.Camera.main.ScreenPointToRay(Input.mousePosition);
        float hitDist = 0.0f;

        if (playerPlane.Raycast(ray, out hitDist))
        {
            Vector3 targetPoint = ray.GetPoint(hitDist);
            Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
            targetRotation.x = 0;
            targetRotation.z = 0;
            playerObj.transform.rotation =
                Quaternion.Slerp(playerObj.transform.rotation, targetRotation, 7f * Time.fixedDeltaTime);
        }

        //Player Movement
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(movementSpeed * Time.fixedDeltaTime * Vector3.forward);
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(movementSpeed * Time.fixedDeltaTime * Vector3.back);
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(movementSpeed * Time.fixedDeltaTime * Vector3.left);
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(movementSpeed * Time.fixedDeltaTime * Vector3.right);
        }

        //Jumping
        if (Input.GetKey(KeyCode.Space)) //&& isGrounded
        {
            transform.Translate(jumpHeight * Time.fixedDeltaTime * Vector3.up);
        }

        //Shooting
        if (Input.GetMouseButton(0) && Time.time - _lastShoot > timeBetweenShoots) // shooting every "timeBetweenShoots" 
        {
            _lastShoot = Time.time;
            Shoot();
        }
    }

    private void Shoot()
    {
        _bulletSpawned = Instantiate(bullet.transform, bulletSpawnPoint.transform.position, Quaternion.identity);
        _bulletSpawned.rotation = bulletSpawnPoint.transform.rotation;
    }
}