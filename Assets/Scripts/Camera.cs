﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    //Variables
    public Transform player;
    public float smooth = 0.3f;
    public float height;

    private Vector3 _velocity = Vector3.zero;


    //Methods

    void FixedUpdate()
    {
        Vector3 pos = new Vector3();
        var position = player.position;
        pos.x = position.x;
        pos.z = position.z - 7f;
        pos.y = position.y + height;

        transform.position = Vector3.SmoothDamp(transform.position, pos, ref _velocity, smooth);
    }
}